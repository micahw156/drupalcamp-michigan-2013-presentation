# Building a Curriculum Management and Publication System using ERP Data and Web Services

Presented at [DrupalCamp Michigan 2013](https://2013.drupalcampmi.org/sessions/building-curriculum-management-and-publication-system-using-erp-data-and-web-services)

Henry Ford Community College's Drupal-based curriculum management system utilizes extract data from the ERP system defined as custom entities to simplify configuration and reduce data entry redundancies. Drupal content on the curriculum management website is then shared with HFCC's public-facing websites using the Services module.

## What this session will cover:
    
* Externally-populated data tables configured as fieldable Drupal entities.
* Drupal content easily exposed using the Services and Services Views modules.
* Custom clients that can retrieve and render this information on other sites.

## What this session will not cover:
    
* **Security:** This system does not use any authentication security. Our back-end web servers are protected by NetIQ Access Manager, and all (well, most) of the content exposed by this system is for public audiences anyway.
* **Every detail:** A lot of the coding required for this system is somewhere near the high end of Intermediate, and won't fit in a 40-minute session. What I hope to cover is a good overview of the elements that need to be built, with links to enough tutorials and examples that you can see what's possibile and then muddle through something similar without making a lot of the same mistakes I have.

## Creating Custom Entities:

This section will cover the basics of defining custom entities and discuss what is required to make them fieldable, editable and ready for display alone, with Views or as options in select lists.

## Providing Web Services:

This section will introduce the Services and Services Views modules, then show how to write custom hook code for better performance.

## Making a Web Services Client:

This section will show how easy it can be to retrieve data from another website by taking advantage of the Services module's ability to send serialized PHP arrays and objects. We’ll also look at a fake field formatter so your custom data can be themed to look just like regular Drupal fields.

You can follow along with this session at [http://chacadwa.com/dcmi13](http://chacadwa.com/dcmi13)

Length: 40 minutes  
Experience Level: Intermediate

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
